
(function ($) {

    Drupal.modalFrameAnonNodeCreate = Drupal.modalFrameAnonNodeCreate || {};

    /**
 * Opens Login and Register links in a Modal Frame.
 */
    Drupal.behaviors.modalFrameAnonNodeCreate = function(context) {       
    
     $('input.modalframe-register:not(.modalframe-register-processed)', context)
    
        // Now that we have matched an element, we add a CSS class, so that
        // we do not process the same element more than once.
        .addClass('modalframe-register-processed')

        // Now, we attach an onClick event handler to the element.
        // attach save form event
        //.click(Drupal.modalFrameRegister.saveFormData)
    
        .click(Drupal.modalFrameRegister.onClick);  


        $('input.modalframe-anon-node-create:not(.modalframe-anon-node-create-processed)', context)
    
        // Now that we have matched an element, we add a CSS class, so that
        // we do not process the same element more than once.
        .addClass('modalframe-anon-node-create-processed')

        // Now, we attach an onClick event handler to the element.
        // attach save form event
        .click(Drupal.modalFrameAnonNodeCreate.saveFormData);
    
    
        // add handlers for the textboxes repopulate it.
        //edit-title
        $('#edit-body').ready(Drupal.modalFrameAnonNodeCreate.rePopFormData);
    
    
    };
    


 
 /**
 * onClick event handler.
 */

Drupal.modalFrameAnonNodeCreate.saveFormData = function() {
        
     //alert("saving" +$('#edit-title').val());
     // save off the form values   
   //  $.cookie('#edit-title', $('#edit-title').val());
     
    // $.cookie('#edit-body', $('#edit-body').val());
   
        // use foreach 
        // iterate through all  of these:
        // regular expression to match either input
        // or textarea for the body section. 
        $('(input|textarea)[id^="edit"]').each(function(index) {
            //alert(index + ': ' + $(this).attr('id') + " val => " + $(this).val());
            // save off previous values
            //alert("save found" + $(this).val().indexOf('Save'));
            if(($(this).val()!=null || $(this).val()!= '') && $(this).val().indexOf('Save') == -1){
                $.cookie($(this).attr('id'), $(this).val());
            }    
        });
  
 
 
    };

    Drupal.modalFrameAnonNodeCreate.rePopFormData = function() {   
       // alert("restoring" + $.cookie('#edit-title'));
        /*$('#edit-title').val($.cookie('#edit-title'));
        $('#edit-body').val($.cookie('#edit-body'));
        
        // clear cookies
         $.cookie('#edit-title', null);
         $.cookie('#edit-body', null);
        */
        
        
        $('(input|textarea)[id^="edit"]').each(function(index) {
            if($.cookie($(this).attr('id')) !=null){  
                //alert("restore " + index + ': ' + $(this).attr('id') + " val => " + $(this).val());       
                $(this).val($.cookie($(this).attr('id')));
                // clear out the cookie
                $.cookie($(this).attr('id'), null);    
            }
        });        
    }
})(jQuery);
